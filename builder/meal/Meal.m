//
//  Meal.m
//  DesignPatterns
//
//  Created by weixq on 2022/2/17.
//

#import "Meal.h"
#import "Item.h"

@interface Meal()
@property (nonatomic, strong) NSMutableArray <id <Item>> *items;
@end

@implementation Meal

- (instancetype)init{
    self = [super init];
    if (self) {
        _items = [[NSMutableArray alloc] init];
    }
    return self;
}

// 点餐
- (void)addItem:(id <Item>)item{
    [_items addObject:item];
}

// 结算
- (float)getCost{
    float cost = 0.f;
    for (id <Item> item in _items) {
        cost += item.price;
    }
    return cost;
}

// 展示
- (void)showItems{
    for (id <Item> item in _items) {
        NSString *name = [item name];
        NSString *packing = [[item packing] pack];
        float price = [item price];
        NSLog(@"Item : %@, Packing : %@, Price : %.1f", name, packing, price);
    }
}

@end
