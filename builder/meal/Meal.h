//
//  Meal.h
//  DesignPatterns
//
//  Created by weixq on 2022/2/17.
//

#import <Foundation/Foundation.h>
#import "Item.h"

NS_ASSUME_NONNULL_BEGIN


/// 单点
@interface Meal : NSObject

// 点单
- (void)addItem:(id <Item>)item;

// 结算
- (float)getCost;

// 展示
- (void)showItems;

@end

NS_ASSUME_NONNULL_END
