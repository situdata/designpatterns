//
//  MealBuilder.h
//  DesignPatterns
//
//  Created by weixq on 2022/2/17.
//

#import <Foundation/Foundation.h>
#import "Meal.h"

NS_ASSUME_NONNULL_BEGIN

/// 套餐
@interface MealBuilder : NSObject


/// 素食套餐
- (Meal *)prepareVegMeal;

/// 非素食套餐
- (Meal *)prepareNonVegMeal;

@end

NS_ASSUME_NONNULL_END
