//
//  MealBuilder.m
//  DesignPatterns
//
//  Created by weixq on 2022/2/17.
//

#import "MealBuilder.h"
#import "VegBurger.h"
#import "Coke.h"
#import "ChickenBurger.h"
#import "Pepsi.h"

@implementation MealBuilder

// 素食套餐
- (Meal *)prepareVegMeal{
    Meal *meal = [[Meal alloc] init];
    [meal addItem:[[VegBurger alloc] init]];
    [meal addItem:[[Coke alloc] init]];
    return meal;
}

// 非素食套餐
- (Meal *)prepareNonVegMeal{
    Meal *meal = [[Meal alloc] init];
    [meal addItem:[[ChickenBurger alloc] init]];
    [meal addItem:[[Pepsi alloc] init]];
    return meal;
}

@end
