//
//  Item.h
//  DesignPatterns
//
//  Created by weixq on 2022/2/17.
//

#import <Foundation/Foundation.h>
#import "Packing.h"

NS_ASSUME_NONNULL_BEGIN

/// 协议类-食物条目
@protocol Item <NSObject>

@optional

/// 食物名
- (NSString *)name;

/// 包装
- (id <Packing>)packing;

/// 价格
- (float)price;

@end

NS_ASSUME_NONNULL_END
