//
//  Packing.h
//  DesignPatterns
//
//  Created by weixq on 2022/2/17.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// 协议类-包装
@protocol Packing <NSObject>

/// 包装
- (NSString *)pack;

@end

NS_ASSUME_NONNULL_END
