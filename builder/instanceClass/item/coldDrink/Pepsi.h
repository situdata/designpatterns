//
//  Pepsi.h
//  DesignPatterns
//
//  Created by weixq on 2022/2/17.
//

#import "ColdDrink.h"

NS_ASSUME_NONNULL_BEGIN

@interface Pepsi : ColdDrink

@end

NS_ASSUME_NONNULL_END
