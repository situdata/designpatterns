//
//  Coke.m
//  DesignPatterns
//
//  Created by weixq on 2022/2/17.
//

#import "Coke.h"

@implementation Coke

// 单价
-(float)price{
    return 30.f;
}

// 菜单名
- (NSString *)name{
    return @"Coke";
}

@end
