//
//  ColdDrink.m
//  DesignPatterns
//
//  Created by weixq on 2022/2/17.
//

#import "ColdDrink.h"
#import "Packing.h"
#import "Bottle.h"

@implementation ColdDrink

// 打包方式
-(id<Packing>)packing {
    return [[Bottle alloc] init];
}

@end
