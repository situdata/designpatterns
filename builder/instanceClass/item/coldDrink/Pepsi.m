//
//  Pepsi.m
//  DesignPatterns
//
//  Created by weixq on 2022/2/17.
//

#import "Pepsi.h"

@implementation Pepsi

// 单价
-(float)price{
    return 35.f;
}

// 菜单名
- (NSString *)name{
    return @"Pepsi";
}

@end
