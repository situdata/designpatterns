//
//  ColdDrink.h
//  DesignPatterns
//
//  Created by weixq on 2022/2/17.
//

#import <Foundation/Foundation.h>
#import "Item.h"

NS_ASSUME_NONNULL_BEGIN

@interface ColdDrink : NSObject <Item>

@end

NS_ASSUME_NONNULL_END
