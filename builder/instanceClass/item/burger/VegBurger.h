//
//  VegBurger.h
//  DesignPatterns
//
//  Created by weixq on 2022/2/17.
//

#import "Burger.h"

NS_ASSUME_NONNULL_BEGIN

@interface VegBurger : Burger

@end

NS_ASSUME_NONNULL_END
