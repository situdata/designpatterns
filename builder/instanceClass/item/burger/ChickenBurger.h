//
//  ChickenBurger.h
//  DesignPatterns
//
//  Created by weixq on 2022/2/17.
//

#import "Burger.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChickenBurger : Burger

@end

NS_ASSUME_NONNULL_END
