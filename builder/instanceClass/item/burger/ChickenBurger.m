//
//  ChickenBurger.m
//  DesignPatterns
//
//  Created by weixq on 2022/2/17.
//

#import "ChickenBurger.h"

@implementation ChickenBurger

// 单价
-(float)price{
    return 50.f;
}

// 菜单名
- (NSString *)name{
    return @"Chicken Burger";
}


@end
