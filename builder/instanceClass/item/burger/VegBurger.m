//
//  VegBurger.m
//  DesignPatterns
//
//  Created by weixq on 2022/2/17.
//

#import "VegBurger.h"

@implementation VegBurger

// 单价
-(float)price{
    return 25.f;
}

// 菜单名
- (NSString *)name{
    return @"Veg Burger";
}

@end
