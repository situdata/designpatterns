//
//  Bottle.h
//  DesignPatterns
//
//  Created by weixq on 2022/2/17.
//

#import <Foundation/Foundation.h>
#import "Packing.h"

NS_ASSUME_NONNULL_BEGIN

/// 实现协议<Packing>的实体类-瓶装
@interface Bottle : NSObject <Packing>

@end

NS_ASSUME_NONNULL_END
