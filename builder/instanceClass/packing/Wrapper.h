//
//  Wrapper.h
//  DesignPatterns
//
//  Created by weixq on 2022/2/17.
//

#import <Foundation/Foundation.h>
#import "Packing.h"

NS_ASSUME_NONNULL_BEGIN

/// 实现了协议<Packing>的实体类-纸包装
@interface Wrapper : NSObject <Packing>

@end

NS_ASSUME_NONNULL_END
