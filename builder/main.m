//
//  main.m
//  builder
//
//  Created by weixq on 2022/2/21.
//

#import <Foundation/Foundation.h>
#import "MealBuilder.h"
#import "Meal.h"
#import "Common.h"

#pragma mark 函数声明

/// 建造者模式
void builderDemo(void);

#pragma mark - main

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        //4. 建造者Demo
        builderDemo();
    }
    return 0;
}

#pragma mark - 建造者模式

void builderDemo(void){

    // 准备素食套餐
    MealBuilder *mealBuilder = [[MealBuilder alloc] init];
    Meal *vegMeal = mealBuilder.prepareVegMeal;
    NSLog(@"veg Meal");
    [vegMeal showItems];
    NSLog(@"%f.1", vegMeal.getCost);

    // 准备非素食套餐
    Meal *nonVegMeal = mealBuilder.prepareNonVegMeal;
    NSLog(@"Non-Veg Meal");
    [nonVegMeal showItems];
    NSLog(@"%f.1", nonVegMeal.getCost);

    printLine();
}
