//
//  ColorFactory.m
//  DesignPatterns
//
//  Created by weixq on 2022/2/16.
//

#import "ColorFactory.h"
#import "Red.h"
#import "Green.h"
#import "Blue.h"

@implementation ColorFactory

- (id<Color>)getColor:(nonnull NSString *)color {
    if(color == nil){
        return nil;
    }
    if([color isEqualToString:@"RED"]){
        return [[Red alloc] init];;
    } else if([color isEqualToString:@"GREEN"]){
        return [[Green alloc] init];
    } else if([color isEqualToString:@"BLUE"]){
        return [[Blue alloc] init];
    }
    return nil;
}

- (id<Shape>)getShape:(nonnull NSString *)shape {
    return nil;
}

@end
