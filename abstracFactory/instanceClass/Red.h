//
//  Red.h
//  DesignPatterns
//
//  Created by weixq on 2022/2/16.
//

#import <Foundation/Foundation.h>
#import "Color.h"

NS_ASSUME_NONNULL_BEGIN


/// 实现<Color>协议的的实体类-红色
@interface Red : NSObject <Color>

@end

NS_ASSUME_NONNULL_END
