//
//  Green.h
//  DesignPatterns
//
//  Created by weixq on 2022/2/16.
//

#import <Foundation/Foundation.h>
#import "Color.h"

NS_ASSUME_NONNULL_BEGIN


/// 实现协议<Color> 的实体类-绿色
@interface Green : NSObject <Color>

@end

NS_ASSUME_NONNULL_END
