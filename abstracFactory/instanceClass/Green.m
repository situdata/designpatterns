//
//  Green.m
//  DesignPatterns
//
//  Created by weixq on 2022/2/16.
//

#import "Green.h"

@implementation Green

- (void)fill{
    NSLog(@"Inside Green::fill() method.");
}

@end
