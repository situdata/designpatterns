//
//  Red.m
//  DesignPatterns
//
//  Created by weixq on 2022/2/16.
//

#import "Red.h"

@implementation Red

- (void)fill{
    NSLog(@"Inside Red::fill() method.");
}

@end
