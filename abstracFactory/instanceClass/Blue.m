//
//  Blue.m
//  DesignPatterns
//
//  Created by weixq on 2022/2/16.
//

#import "Blue.h"

@implementation Blue

- (void)fill{
    NSLog(@"Inside Blue::fill() method.");
}

@end
