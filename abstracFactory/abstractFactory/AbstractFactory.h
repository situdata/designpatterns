//
//  AbstractFactory.h
//  DesignPatterns
//
//  Created by weixq on 2022/2/16.
//

#import <Foundation/Foundation.h>
#import "Shape.h"
#import "Color.h"

NS_ASSUME_NONNULL_BEGIN

/// 抽象工厂
@protocol AbstractFactory <NSObject>

@required

/// 获取形状工厂
/// @param shape 值: CIRCLE, RECTANGLE, SQUARE"
- (id <Shape>)getShape:(NSString *)shape;

/// 获取颜色工厂
/// @param color  值：RED, GREEN, BLUE
- (id <Color>)getColor:(NSString *)color;

@end

NS_ASSUME_NONNULL_END
