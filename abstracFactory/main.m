//
//  main.m
//  abstracFactory
//
//  Created by weixq on 2022/2/21.
//

#import <Foundation/Foundation.h>
#import "FactoryProducer.h"
#import "ShapeFactory.h"
#import "Common.h"

#pragma mark 函数声明

/// 抽象工厂
void abstractFactory(void);

#pragma mark - main

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        //2. 抽象工厂Demo
        abstractFactory();
    }
    return 0;
}

#pragma mark - 抽象工厂

void abstractFactory(void){
    // 获取形状工厂
    id <AbstractFactory> shapeFactory = [FactoryProducer getFactory:@"SHAPE"];

    // 获取形状为 Circle 的对象
    id <Shape> shape1 = [shapeFactory getShape:@"CIRCLE"];
    [shape1 draw];

    // 获取形状为 Rectangle 的对象
    id <Shape> shape2 = [shapeFactory getShape:@"RECTANGLE"];
    [shape2 draw];

    // 获取形状为 Square 的对象
    id <Shape> shape3 = [shapeFactory getShape:@"SQUARE"];
    [shape3 draw];

    // 获取颜色工厂
    id <AbstractFactory> colorFactory = [FactoryProducer getFactory:@"COLOR"];

    //获取颜色为 Red 的对象
    id <Color> color1 = [colorFactory getColor:@"RED"];
    [color1 fill];

    //获取颜色为 Green 的对象
    id <Color> color2 = [colorFactory getColor:@"GREEN"];
    [color2 fill];

    //获取颜色为 Blue 的对象
    id <Color> color3 = [colorFactory getColor:@"BLUE"];
    [color3 fill];

    printLine();
}



