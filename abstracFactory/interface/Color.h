//
//  Color.h
//  DesignPatterns
//
//  Created by weixq on 2022/2/16.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


/// 协议类-颜色
@protocol Color <NSObject>

@required
- (void)fill;

@end

NS_ASSUME_NONNULL_END
