//
//  FactoryProducer.m
//  DesignPatterns
//
//  Created by weixq on 2022/2/16.
//

#import "FactoryProducer.h"
#import "ShapeFactory.h"
#import "ColorFactory.h"

@implementation FactoryProducer

+ (id<AbstractFactory>)getFactory:(NSString *)choice{
    if ([choice isEqualToString:@"SHAPE"]) {
        return [[ShapeFactory alloc] init];
    } else if ([choice isEqualToString:@"COLOR"]){
        return [[ColorFactory alloc] init];
    } else {
        return nil;
    }
}

@end
