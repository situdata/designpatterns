//
//  FactoryProducer.h
//  DesignPatterns
//
//  Created by weixq on 2022/2/16.
//

#import <Foundation/Foundation.h>
#import "AbstractFactory.h"

NS_ASSUME_NONNULL_BEGIN


/// 工厂创造器/生成器类，通过传递形状或颜色信息来获取工厂。
@interface FactoryProducer : NSObject

/// 获取工厂
/// @param choice  值：SHAPE， SHAPE
+ (id <AbstractFactory>)getFactory:(NSString *)choice;

@end

NS_ASSUME_NONNULL_END
