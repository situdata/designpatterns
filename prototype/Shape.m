//
//  Shape.m
//  prototype
//
//  Created by weixq on 2022/2/21.
//

#import "Shape.h"

@interface Shape()
@property (nonatomic, assign) NSString *id;
@property (nonatomic, copy) NSString *type;
@end

@implementation Shape

@end
