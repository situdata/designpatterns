//
//  STDrawBoard.h
//  TruthEyeiOS4ZXBC
//
//  Created by 辛巨文 on 2021/7/6.
//  Copyright © 2021 刘一. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol STDrawBoardDelegate <NSObject>

-(void)didGetSingImage:(UIImage *)image;

@end


@interface STDrawBoard : UIView
@property(nonatomic,weak) id<STDrawBoardDelegate> delegate;//此数组用来管理画板上所有的路径
//生成图片
- (UIImage *)getImage;

//清空内容
-(void)clear;

//画板上是否有内容
-(BOOL)isHasContent;

@end

NS_ASSUME_NONNULL_END
