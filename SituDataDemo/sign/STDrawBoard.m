//
//  STDrawBoard.m
//  TruthEyeiOS4ZXBC
//
//  Created by 辛巨文 on 2021/7/6.
//  Copyright © 2021 刘一. All rights reserved.
//

#import "STDrawBoard.h"
#import "MyUIBezierPath.h"

@interface STDrawBoard()

@property(nonatomic,strong) NSMutableArray *paths;//此数组用来管理画板上所有的路径
@property(nonatomic,assign)CGPoint point1; //截取签字的坐标最低点
@property(nonatomic,assign)CGPoint point2; //截取签字的坐标最高点 截取签字图片的大小
@end

@implementation STDrawBoard

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColor.clearColor;
    }
    return self;
}

//清空内容
-(void)clear {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(didGetSingImage) object:nil];
    [self.paths removeAllObjects];
    //重绘
    [self setNeedsDisplay];
}

-(NSArray *)paths{
    if(!_paths){
        _paths=[NSMutableArray array];
    }
    return _paths;
}

//生成图片
- (UIImage *)getImage {
    UIGraphicsBeginImageContext(self.bounds.size);
//    //设置一个透明的颜色
//    UIColor * color = [UIColor clearColor];
//    //使用上面设置的颜色进行填充.
//    [color setFill];
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //截取所需区域
    CGRect captureRect = CGRectMake(self.point1.x,self.point1.y,self.point2.x-self.point1.x,self.point2.y-self.point1.y);
    CGImageRef sourceImageRef = [image CGImage];
    CGImageRef newImageRef = CGImageCreateWithImageInRect(sourceImageRef, captureRect);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    UIImageWriteToSavedPhotosAlbum(newImage, self, @selector(imageSavedToPhotosAlbum:didFinishSavingWithError:contextInfo:), nil);
    UIImage *resultImg = [self composeImg:newImage];
//    //保存到相册
    UIImageWriteToSavedPhotosAlbum(resultImg, self, @selector(imageSavedToPhotosAlbum:didFinishSavingWithError:contextInfo:), nil);
//
    return resultImg;
}

//裁剪的图片放到320*200的底部白色背景图上
- (UIImage *)composeImg:(UIImage *)img{
    CGSize size = CGSizeMake(300, 200);
    UIGraphicsBeginImageContext(size);
    
//    //设置一个透明的颜色
//    UIColor * color = [UIColor clearColor];
//    //使用上面设置的颜色进行填充.
//    [color setFill];
    
    float orginX = 0;
    float orginY = 0;
    float width = 300;
    float height = 200;
    
    //裁剪图片宽高比  实际压缩 居中显示
    if (img.size.width>300 || img.size.height>200) {
        if (img.size.width/img.size.height>3/2) {
            width = 300;
            height = img.size.height*(300/img.size.width);
            
            orginY = (200 - height)/2;
        }else{
            height = 200;
            width = img.size.width*(200/img.size.height);
            
            orginX = (300 - width)/2;
        }
    }
    
    if (img.size.width<300 && img.size.height<200) {
        orginX = (300 - img.size.width)/2;
        orginY = (200 - img.size.height)/2;
        
        width = img.size.width;
        height = img.size.height;
    }
    
    [img drawInRect:CGRectMake(orginX, orginY, width, height)];
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resultImage;
}

 
//保存图片的回调
- (void)imageSavedToPhotosAlbum:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    NSString *message = @"";
    if (!error) {
        message = @"成功保存到相册";
    }else{
        message = [error description];
    }
    NSLog(@"message is %@",message);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(didGetSingImage) object:nil];
    // 获取触摸对象
    UITouch *touch=[touches anyObject];
    // 获取手指的位置
    CGPoint point=[touch locationInView:touch.view];
    
    if (self.point1.x==0) {
        self.point1 = point;
    }
    
    if (point.x<self.point1.x) {
        float y = self.point1.y;
        self.point1 = CGPointMake(point.x-9, y);
    }
    
    if (point.y<self.point1.y) {
        float x = self.point1.x;
        self.point1 = CGPointMake(x, point.y-9);
    }
    
    //当手指按下的时候就创建一条路径
    MyUIBezierPath *path=[MyUIBezierPath bezierPath];
    //设置画笔宽度
    [path setLineWidth:9];
    //设置画笔颜色
    [path setLineColor:UIColor.blackColor];
    //设置起点
    [path moveToPoint:point];
    // 把每一次新创建的路径 添加到数组当中
    [self.paths addObject:path];
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    // 获取触摸对象
    UITouch *touch=[touches anyObject];
    // 获取手指的位置
    CGPoint point=[touch locationInView:touch.view];
    
    if (point.x<self.point1.x) {
        float y = self.point1.y;
        self.point1 = CGPointMake(point.x-9, y);
    }
    
    if (point.y<self.point1.y) {
        float x = self.point1.x;
        self.point1 = CGPointMake(x, point.y-9);
    }
    
    if (point.x>self.point2.x) {
        float y = self.point2.y;
        self.point2 = CGPointMake(point.x+9, y);
    }
    
    if (point.y>self.point2.y) {
        float x = self.point2.x;
        self.point2 = CGPointMake(x, point.y+9);
    }
    
    // 连线的点
    [[self.paths lastObject] addLineToPoint:point];
    // 重绘
    [self setNeedsDisplay];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch=[touches anyObject];
    // 获取手指的位置
    CGPoint point=[touch locationInView:touch.view];
    
    if (point.x>self.point2.x) {
        float y = self.point2.y;
        self.point2 = CGPointMake(point.x+9, y);
    }
    
    if (point.y>self.point2.y) {
        float x = self.point2.x;
        self.point2 = CGPointMake(x, point.y+9);
    }
    
    [self performSelector:@selector(didGetSingImage) withObject:nil afterDelay:0.5];
}


- (void)drawRect:(CGRect)rect {
//    [[UIColor clearColor] set];
    // Drawing code
    for (MyUIBezierPath *path in self.paths) {
        //设置颜色
        [path.lineColor set];
        // 设置连接处的样式
        [path setLineJoinStyle:kCGLineJoinRound];
        // 设置头尾的样式
        [path setLineCapStyle:kCGLineCapRound];
        //渲染
        [path stroke];
    }
}

//画板上是否有内容
-(BOOL)isHasContent {
    return self.paths.count > 0;
}

//生成图片，给代理类
-(void)didGetSingImage {
    if ([self.delegate respondsToSelector:@selector(didGetSingImage:)]) {
        [self.delegate didGetSingImage:[self getImage]];
        [self clear];
    }
}

@end
