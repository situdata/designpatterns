//
//  SceneDelegate.h
//  SituDataDemo
//
//  Created by weixq on 2022/2/15.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

