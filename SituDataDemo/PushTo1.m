//
//  PushTo1.m
//  SituDataDemo
//
//  Created by weixq on 2022/2/28.
//

#import "PushTo1.h"

@interface PushTo1 ()
@property (nonatomic, strong) UIScrollView *scrollView;
@end

@implementation PushTo1

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;
    self.title = @"pushTo";
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    self.scrollView.contentSize = CGSizeMake(CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)*2);
    [self.view addSubview:self.scrollView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeClose];
    [btn addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    if (@available(iOS 15.0, *)) {
        UINavigationBarAppearance *appearnce = [[UINavigationBarAppearance alloc] init];
        appearnce.backgroundColor = UIColor.redColor;
        self.navigationController.navigationBar.standardAppearance = appearnce;
        self.navigationController.navigationBar.scrollEdgeAppearance = appearnce;
    }else{
        // 设置no，状态栏使用view的颜色，yes为透明
        self.navigationController.navigationBar.translucent = NO;
        self.navigationController.navigationBar.backgroundColor = UIColor.redColor;
    }

    //设置阴影的高度
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0, 3);
    //设置透明度
    self.navigationController.navigationBar.layer.shadowOpacity = 0.2;
    self.navigationController.navigationBar.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.navigationController.navigationBar.bounds].CGPath;
}

- (void)close{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
