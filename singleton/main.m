//
//  main.m
//  singleton
//
//  Created by weixq on 2022/2/21.
//

#import <Foundation/Foundation.h>
#import "ShareManager.h"

#pragma mark 函数声明

/// 单例模式
void singletonDemo(void);

#pragma mark - main

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        //3. 单例Demo
        singletonDemo();
    }
    return 0;
}

#pragma mark - 单例模式

void singletonDemo(void){
    ShareManager *share1 = [ShareManager sharedInstance];
    NSLog(@"share1 %@", share1);
    ShareManager *share2 = [ShareManager sharedInstance];
    NSLog(@"share2 %@", share2);
}
