//
//  ShareManager.h
//  DesignPatterns
//
//  Created by weixq on 2022/2/16.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShareManager : NSObject

/// 获取单例对象
+ (instancetype)sharedInstance;

@end

NS_ASSUME_NONNULL_END
