//
//  ShapeFactory.m
//  DesignPatterns
//
//  Created by weixq on 2022/2/15.
//

#import "ShapeFactory.h"
#import "Circle.h"
#import "Square.h"
#import "Rectangle.h"


@implementation ShapeFactory

- (id <Shape>)getShape:(NSString *)shapeType;{
    if(shapeType == nil){
        return nil;
    }
    if([shapeType isEqualToString:@"CIRCLE"]){
        return [[Circle alloc] init];;
    } else if([shapeType isEqualToString:@"RECTANGLE"]){
        return [[Rectangle alloc] init];
    } else if([shapeType isEqualToString:@"SQUARE"]){
        return [[Square alloc] init];
    }
    return nil;
}

- (id<Color>)getColor:(NSString *)color{
    return nil;
}

@end
