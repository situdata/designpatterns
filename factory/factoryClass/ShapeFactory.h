//
//  ShapeFactory.h
//  DesignPatterns
//
//  Created by weixq on 2022/2/15.
//

#import <Foundation/Foundation.h>
#import "Shape.h"
#import "AbstractFactory.h"

NS_ASSUME_NONNULL_BEGIN

/// 工厂类，生成基于给定信息的实体类对象
@interface ShapeFactory : NSObject <AbstractFactory>

@end

NS_ASSUME_NONNULL_END
