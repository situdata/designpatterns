//
//  Shape.h
//  DesignPatterns
//
//  Created by weixq on 2022/2/15.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// 协议类-形状
@protocol Shape <NSObject>

@required
- (void)draw;

@end

NS_ASSUME_NONNULL_END
