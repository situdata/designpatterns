//
//  main.m
//  factory
//
//  Created by weixq on 2022/2/21.
//

#import <Foundation/Foundation.h>
#import "ShapeFactory.h"
#import "Common.h"

#pragma mark 函数声明

/// 工厂模式
void factoryPatterm(void);

#pragma mark - main

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        //1. 工厂模式Demo
        factoryPatterm();
    }
    return 0;
}

#pragma mark - 工厂模式

void factoryPatterm(void) {
    ShapeFactory *shapeFactory = [[ShapeFactory alloc] init];

    //获取 Circle 的对象，并调用它的 draw 方法
    id <Shape> shape1  = [shapeFactory getShape:@"CIRCLE"];
    [shape1 draw];

    //获取 Rectangle 的对象，并调用它的 draw 方法
    id <Shape> shape2  = [shapeFactory getShape:@"RECTANGLE"];
    [shape2 draw];

    //获取 Square 的对象，并调用它的 draw 方法
    id <Shape> shape3  = [shapeFactory getShape:@"SQUARE"];
    [shape3 draw];

    printLine();
}


