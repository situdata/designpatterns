//
//  Square.m
//  DesignPatterns
//
//  Created by weixq on 2022/2/15.
//

#import "Square.h"


@implementation Square 

- (void)draw {
    NSLog(@"Inside Square::draw() method.");
}

@end
