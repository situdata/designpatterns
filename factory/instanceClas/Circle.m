//
//  Circle.m
//  DesignPatterns
//
//  Created by weixq on 2022/2/15.
//

#import "Circle.h"

@implementation Circle

- (void)draw {
    NSLog(@"Inside Circle::draw() method.");
}

@end
