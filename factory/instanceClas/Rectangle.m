//
//  Rectangle.m
//  DesignPatterns
//
//  Created by weixq on 2022/2/15.
//

#import "Rectangle.h"

@implementation Rectangle

- (void)draw { 
    NSLog(@"Inside Rectangle::draw() method.");
}

@end
