//
//  Rectangle.h
//  DesignPatterns
//
//  Created by weixq on 2022/2/15.
//

#import <Foundation/Foundation.h>
#import "Shape.h"

NS_ASSUME_NONNULL_BEGIN


/// 实现<Shape>协议的实体类-长方形
@interface Rectangle : NSObject <Shape>

@end

NS_ASSUME_NONNULL_END
